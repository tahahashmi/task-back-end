// Import Mongoose Module so that we can connect to our Database 
const mongoose = require('mongoose')
//Use Mongoose connect function to connect to DB
mongoose.connect('mongodb://localhost:27017/Task',(err)=>{
    //If there is no Error in connecting to DB
    if(!err){
        console.log("Successfully connected to Database ");
    }
    //Else Display the Error Message 
    else{
        console.log("Error in DB connection : "+JSON.stringify(err))
    }
});
// export this module so that we can use this in our startup class 
module.exports = mongoose;