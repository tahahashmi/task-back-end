//This is out Model class here we define what our model (article) should be and its properties 
const mongoose = require('mongoose');
var Article = mongoose.model('Article',{
    title:{type:String},
    description:{type:String},
    image:{type:String},
    createdAt:{type:Date}
})

module.exports = {Article}