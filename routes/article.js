//This is the main controller for handling all the HTTP requests
const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;
var requestHelper = require("../helpers/request");
var {Article} = require('../models/article');


// This will handle the  HTTP GET request to fetch all the articles present in DB 
router.get('/',(req,res)=>{
    Article.find((err,result)=>{
        if(!err) {res.send(result);}
        else{console.log('Error in Fetching Records  : ' + JSON.stringify(err) )}
    });

});
// This will handle the  HTTP GET request to fetch particular article present in DB 
router.get('/:id',(req,res)=>{
     //First we need to check if the id that is being passed in url is a valid objectID or not 
    if(!ObjectId.isValid(req.params.id)){
        return res.status(400).send(`No record with given id : ${req.params.id}`)
    }
   // if it is a valid object Id then we find that particular record by that id 
    Article.findById(req.params.id,(err,result)=>{
        if(!err){res.send(result);}
        else{console.log('Error in Fetching Data : ' + JSON.stringify(err) )}
    })

});

//This will handle all the HTTP POST request to INSERT new Article in DB 
router.post('/',(req,res)=>{
    //First we parse the request body to JSON with our request helper function
    postBody = requestHelper.parseBody(req.body);
    //then we create a new Article Object and insert all the fields that we got from our request 
    var article = new Article({
        title:postBody.title,
        description:postBody.description,
        image:postBody.image,
        createdAt:postBody.createdAt
    });
    //Now after creating new Article Object we push this object into our DB 
    article.save((err,result)=>{
        if(!err){res.send(result);}
        else{console.log('Error in Saving Data : ' + JSON.stringify(err) )}
    
    })
});
// This will handle out HTTP put request to Update an existing record in DB 
router.put('/:id',(req,res)=>{
  
    //First we need to check if the id that is being passed in url is a valid objectID or not 
    if(!ObjectId.isValid(req.params.id))
    return res.status(400).send(`No Record with given id : ${req.params.id}`)
    //Then we parse the request body to JSON with our request helper function
    putBody = requestHelper.parseBody(req.body);
    // if it is a valid object Id then we find that particular record by that id 
    Article.findById(req.params.id,(err,article)=>{
        // Now we update and save the found record with the object that is being passed in request 
        article.title=putBody.title;
        article.description=putBody.description;
        article.image=putBody.image;
        article.createdAt=putBody.createdAt;
        article.save();
        res.json(article);
    })
    

});
// This will handle out HTTP Delete request to Delete an existing record in DB 
router.delete('/:id',(req,res)=>{ 
    //First we need to check if the id that is being passed in url is a valid objectID or not 
    if(!ObjectId.isValid(req.params.id)){
        return res.status(400).send('No record with given id : ${req.params.id}');
    }
     // if it is a valid object Id then we find that particular record by that id  and delete it
    Article.findByIdAndRemove(req.params.id , (err,doc)=>{
        if(!err){res.send(doc);}
        else{console.log('Error in Deleting Data : ' + JSON.stringify(err) )}  
    })
})



module.exports = router;
